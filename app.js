import { MongoClient } from "mongodb";

const client = new MongoClient(`mongodb://mongodb`)
try {
    await client.connect()
    const collections = await client.db('gps65').listCollections().toArray()
    console.log(collections)
} catch (e) {
    console.log(e)
}

console.log('done')
